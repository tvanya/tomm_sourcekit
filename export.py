# python

import os, subprocess, lx

s = lx.Service( "layerservice" )
s.select( "layers", "fg" )

# layer name
name = s.query( "layer.name" )

# directory where our script will save files
path = os.path.join(lx.eval('user.value project_path ?'), 'src', 'models')
mapname = lx.eval('user.value mapname ?')
surfaceprop = lx.eval('user.value surfaceprop ?')

X = lx.eval("user.value x ?")
Y = lx.eval("user.value y ?")
Z = lx.eval("user.value z ?")

# we're getting just the name here, not the surfaceprop e.g.(mesh:surface)
if ':' in name:
  p           = name.split(':', 1)
  name        = p[0]
  surfaceprop = p[1]
else:
  pass

# actual file directory
directory = os.path.join(path, name, 'compile')

csgo_path = 'C:\Program Files (x86)\Steam\steamapps\common\Counter-Strike Global Offensive'
bin_path = os.path.join(csgo_path, 'bin');
# lx.out(bin_path)

collision = False

args = lx.args()
for arg in args:
    if arg == "collision":
        collision = True
		
def mdlcompile():
  FNULL = open(os.devnull, 'w')
  filePath = os.path.join(directory, name + '.qc')
  studiomdl = os.path.join(bin_path, 'studiomdl.exe')
  args = [studiomdl, '-game', os.path.join(csgo_path, 'csgo'), '-nop4', filePath]
  if os.path.exists(filePath):
	  #lx.out(args)
	  subprocess.call(args, stdout=FNULL, stderr=FNULL, shell=False)

def newScene():
  # get selected items from item list
  selected = lx.evalN('query sceneservice selection ? mesh')

  # store original scene
  oldscene = lx.eval('query sceneservice scene.index ? current')

  # loop through selected items and do stuff
  for layer in selected:
	# create new scene and store in variable
	lx.eval('scene.new')
	newscene = lx.eval('query sceneservice scene.index ? current')
	# go back to old scene and throw item over to new scene
	lx.eval('scene.set %s' % oldscene)
	lx.eval('select.item {%s} add' % layer)
	lx.eval('layer.import %s {} move:false position:0' % newscene)

	lstLayers = lx.eval('query layerservice layers ? all')
	firstLayer = lx.eval('query layerservice layer.ID ? %s' % (lstLayers[1]))

	lx.eval('select.subItem %s set mesh' % firstLayer)
	lx.eval('transform.reset translation')
	lx.eval('tool.set TransformRotate on')
	if X:
		lx.eval('transform.channel rot.X 90.0')
	if Y:
		lx.eval('transform.channel rot.Y 90.0')
	if Z:
		lx.eval('transform.channel rot.Z 90.0')
	lx.eval('tool.doApply')
	lx.eval('poly.triple')

def saveScene():
  if os.path.exists(directory):
    filePath = os.path.join(directory, name + '.obj')
    lx.eval('!!scene.saveAs "%s" wf_OBJ false' % (filePath))
  else:
    sys.exit( "LXe_FAILED:Ooops" )

def exportScene():
  if not os.path.exists(directory):
    lx.out('successfully exported to: %s' % directory)
    os.makedirs(directory)
    saveScene()
  else:
      lx.out('successfully exported to(overriding): %s' % directory)
      saveScene()

def generateqc():
  filePath = os.path.join(directory, name + '.qc')

  qc = [
    '$modelname "%s\%s.mdl"' % (mapname ,name),
    '$body mybody "%s.obj"' % (name),
    '$staticprop',
    '$surfaceprop %s' % (surfaceprop),
    '$cdmaterials "models\\%s"' % (mapname),
    '',
    '$sequence idle "%s.obj"' % (name),
    ''
  ]

  if collision:
    qc.append('$collisionmodel "%s_collision.smd" { $concave }' % (name))

  with open(filePath, 'a') as qc_file:
    # clear the file before we start writing
    qc_file.truncate()
    qc_file.write("\n".join(qc))

	
def closeScene():
  lx.eval('!!scene.close')

newScene()
exportScene()
closeScene()
generateqc()
mdlcompile()