# python

import os
import subprocess
import lx

def mat_compile():	
	mapname = lx.eval('user.value mapname ?')

	s = lx.Service( "layerservice" )
	s.select( "layers", "fg" )
	layerName = s.query( "layer.name" )
	
	texture_file = ""
	
	# getting texture name from currently selected layer
	for index in range(lx.eval('query layerservice texture.N ? all')):
		texture_type = lx.eval('query layerservice texture.type ? %s' % index)
		if texture_type == 'imageMap':
			texture_file = lx.eval('query layerservice texture.clipfile ?')

	if 'textures' in texture_file:
		p	= texture_file.split('textures')[1]
	else:
		sys.exit( "LXe_FAILED:There's no material with an image texture applied to \"%s\". \nSupported file extensions: tga, psd." %layerName)
		

	csgo_path = 'C:\Program Files (x86)\Steam\steamapps\common\Counter-Strike Global Offensive'
	bin_path = os.path.join(csgo_path, 'bin')
	vtex = os.path.join(bin_path, 'vtex.exe')

	args = [vtex, '-quiet' ,'-game', os.path.join(csgo_path, 'csgo'), '-outdir', os.path.join(csgo_path, 'csgo', 'materials', 'models', mapname), '-shader', 'VertexLitGeneric', '-nop4', texture_file]
	subprocess.Popen(args, shell=False)

	#lx.out(p)

try:
	# set up the dialog
	lx.eval('dialog.setup yesNo')
	lx.eval('dialog.title {Confirm Operation}')
	lx.eval('dialog.msg {Compile the mesh as well?}')
	lx.eval('dialog.result ok')

	# Open the dialog and see which button was pressed
	lx.eval('dialog.open')
	lx.eval("dialog.result ?")
	mat_compile()
	lx.eval('@export.py')
	
except:
    mat_compile()
